import 'package:flutter/services.dart';
import 'package:soundpool/soundpool.dart';

String formatTime(int seconds, bool pad) {
  return (pad)
      ? (seconds / 60).floor().toString() +
          ":" +
          (seconds % 60).toString().padLeft(2, "0")
      : (seconds > 59)
          ? (seconds / 60).floor().toString() +
              ":" +
              (seconds % 60).toString().padLeft(2, "0")
          : seconds.toString();
}

class SoundPlayer {
  static final Soundpool _soundpool = Soundpool.fromOptions(
      options: const SoundpoolOptions(streamType: StreamType.alarm));
  static int _tickId;
  static int _exEndId;
  static int _woEndId;

  static void loadSounds(
      String tickSound, String exEndSound, String woEndSound) async {
    _tickId = (tickSound != 'none')
        ? await rootBundle
            .load('assets/$tickSound.oga')
            .then((ByteData soundData) => _soundpool.load(soundData))
        : null;
    _exEndId = (exEndSound != 'none')
        ? await rootBundle
            .load('assets/$exEndSound.oga')
            .then((ByteData soundData) => _soundpool.load(soundData))
        : null;
    _woEndId = (woEndSound != 'none')
        ? await rootBundle
            .load('assets/$woEndSound.oga')
            .then((ByteData soundData) => _soundpool.load(soundData))
        : null;
  }

  static void playTickSound() =>
      (_tickId != null) ? _soundpool.play(_tickId) : null;
  static void playExEndSound() =>
      (_exEndId != null) ? _soundpool.play(_exEndId) : null;
  static void playWoEndSound() =>
      (_woEndId != null) ? _soundpool.play(_woEndId) : null;
}
